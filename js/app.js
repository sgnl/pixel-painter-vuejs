
'use strict';

let store = {
  debug: true,
  state: {
    colorSelected: ''
  },
  getColor () {
    this.debug && console.log('getColor triggered');
    return this.state.colorSelected;
  },
  setColor (color) {
    this.debug && console.log('setColor triggered with: ', color);
    return this.state.colorSelected = color;
  }
};

Vue.component('pixel', {
  template: '<div \
    v-bind:style="{\
      backgroundColor: color,\
      height: height,\
      width: width\
    }"\
    @click="setColor"\
  ></div>',
  props: {
    height: {
      type: String,
      default: '10px'
    },
    width: {
      type: String,
      default: '10px'
    }
  },
  data() {
    return {
      color: '#fff'
    }
  },
  methods: {
    setColor() {
      this.color = store.getColor();
    }
  }
});

Vue.component('swatch', {
  template: '<div \
    v-bind:style="{\
      backgroundColor: color,\
      height: height,\
      width: width\
    }"\
    @click="setColor(color)"\
  ></div>',
  props: {
    height: {
      type: String,
      default: '10px'
    },
    width: {
      type: String,
      default: '10px'
    },
    color: {
      type: String,
      default: '#fff'
    }
  },
  methods: {
    setColor(color) {
      return store.setColor(color);
    }
  }
});

var app = new Vue({
  el: '#app',
  data: {
    title: 'Pixel Painter',
    defaultGridColor: 'white',
    colorsAvailable: [
      'yellow',
      'purple',
      'orange'
    ],
    sharedState: store.state
  },
  methods: {
    setColor(color) {
      console.log(`setting ${color}`);
      this.colorSelected = color;
    }
  }
});
